import random

from kivy.uix.widget import Widget
from kivy.core.window import Window
from kivy.app import App
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.clock import Clock
from kivy.core.audio import SoundLoader

from plyer import audio

class MultiSound(object):
    def __init__(self, file, num):
        self.num = num
        self.sounds = [SoundLoader.load(file) for n in range(num)]
        self.index = 0
        
    def play(self):
        self.sounds[self.index].play()
        self.index += 1
        if self.index == self.num:
            self.index = 0
            
        
sfx_move = MultiSound('./sounds/01.wav', 3)
sfx_score = SoundLoader.load('./sounds/02.wav')
sfx_die = SoundLoader.load('./sounds/03.wav')

class Sprite(Image):
    def __init__(self, **kwargs):
        super(Sprite, self).__init__(allow_stretch=True, **kwargs)
        self.size = self.texture_size

        
class Background(Widget):
    def __init__(self, source):
        super(Background, self).__init__()
        self.image = Sprite(source=source)
        self.add_widget(self.image)
        self.size = self.image.size
        self.image_dupe = Sprite(source=source, x=self.width)
        self.add_widget(self.image_dupe)
        
    def update(self):
        self.image.x -= 2
        self.image_dupe.x -= 2
        
        if self.image.right <= 0:
            self.image.x = 0
            self.image_dupe.x = self.width
            
            
class Pole(Widget):
    def __init__(self, pos):
        super(Pole, self).__init__(pos=pos)
        self.top_image = Sprite(source='./images/pole_top.png')
        self.top_image.pos = (self.x, self.y + 1.5 * 204)
        self.add_widget(self.top_image)
        self.bottom_image = Sprite(source='./images/pole_btm.png')
        self.bottom_image.pos = (self.x, self.y - self.bottom_image.height)
        self.add_widget(self.bottom_image)
        self.width = self.top_image.width
        self.scored = False
        
    def update(self):
        self.x -= 2
        self.top_image.x = self.bottom_image.x = self.x
        if self.right < 0:
            self.parent.remove_widget(self)


class Poles(Widget):
    add_pole = 0
    def update(self, dt):
        for child in list(self.children):
            child.update()
        self.add_pole -= dt
        if self.add_pole < 0:
            y = random.randint(self.y + 50, self.height - 50 - 1.5 * 204)
            self.add_widget(Pole(pos=(self.width, y)))
            self.add_pole = 5.5
            

class Calvin(Sprite):
    def __init__(self,pos):
        super(Calvin, self).__init__(source='atlas://./images/calvin/frame2', pos=pos)
        self._keyboard = Window.request_keyboard(self._keyboard_closed, self)
        self._keyboard.bind(on_key_down=self._on_keyboard_down)
        self.velocity_x = 0
        self.velocity_y = 0
        self.suction= -.3
        
    def update(self):
        self.velocity_x += self.suction
        self.velocity_x = max(self.velocity_x, -3)
        self.x += self.velocity_x
        if self.velocity_x < -3:
            self.source = 'atlas://./images/calvin/frame1'
        elif self.velocity_x < -2.5:
            self.source = 'atlas://./images/calvin/frame2'
        elif self.velocity_x < -2:
            self.source = 'atlas://./images/calvin/frame3'
        elif self.velocity_x < -1.5:
            self.source = 'atlas://./images/calvin/frame4'
        elif self.velocity_x < -1:
            self.source = 'atlas://./images/calvin/frame5'
        elif self.velocity_x < 0.5:
            self.source = 'atlas://./images/calvin/frame6'
            
        #self.velocity_y = max(self.velocity_y, -10)
        if self.velocity_y <= 0.3 and self.velocity_y >= -0.3:
            self.velocity_y = 0
        elif self.velocity_y > 0.3:
            self.velocity_y += self.suction
            self.y += self.velocity_y           
        elif self.velocity_y < -0.3:
            self.velocity_y -= self.suction
            self.y += self.velocity_y
        
    def on_touch_down(self, pos, *ignore):
        if pos.x > Window.width / 3:
            self.velocity_x = 8.5
            self.source = 'atlas://./images/calvin/frame6'
#            sfx_move.play()
        elif pos.x < Window.width / 3 and pos.y > Window.height / 2:
            self.velocity_y = 5.5
        elif pos.x < Window.width / 3 and pos.y < Window.height / 2:
            self.velocity_y = -5.5
        
    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down=self._on_keyboard_down)
        self._keyboard = None
        
    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        if keycode[1] == 'w':
            self.velocity_y = 5.5
        if keycode[1] == 's':
            self.velocity_y = -5.5
        

class Game(Widget):
    def __init__(self, **kwargs):
        super(Game, self).__init__(**kwargs)
        # Initialize images using Sprite class to be able to modify Images 
        self.background = Background(source='./images/background.png')
        self.width = self.background.width / 4
        self.height = self.background.height
        self.add_widget(self.background)
        self.poles = Poles(pos=(0, 0), size=self.size)
        self.add_widget(self.poles)
        self.score_label = Label(center_x=self.center_x / 7, top=self.top + 10, text="0")
        self.add_widget(self.score_label)
        self.over_label = Label(center=Window.center, opacity=0, text="GAME OVER")
        self.add_widget(self.over_label)
        self.calvin = Calvin(pos=(self.width / 2, self.height / 2))
        self.add_widget(self.calvin)
        Clock.schedule_interval(self.update, 1.0 / 60.0)
        self.game_over = False
        self.score = 0        
            
    def update(self, dt):
        if self.game_over:
            return
        self.background.update()
        self.calvin.update()
        self.poles.update(dt)
        
        for pole in self.poles.children:
            colBox = Widget(size=(self.calvin.width, self.calvin.height/2), 
                            pos=(self.calvin.center_x-self.calvin.width/2, 
                                 self.calvin.center_y-self.calvin.height/6))
            if pole.top_image.collide_widget(colBox):
                self.game_over = True
            elif pole.bottom_image.collide_widget(colBox):
                self.game_over = True
            elif not pole.scored and pole.right < self.calvin.x:
                pole.scored = True
                self.score += 1
                self.score_label.text = str(self.score)
#                sfx_score.play()
                
        if self.game_over:
            self.over_label.opacity = 1 
            sfx_die.play() 
            self.bind(on_touch_down=self._on_touch_down)
            
    def _on_touch_down(self, *ignore):
        parent = self.parent
        parent.remove_widget(self)
        parent.add_widget(Menu())
        

class Menu(Widget):
    def __init__(self):
        super(Menu, self).__init__()
        self.add_widget(Sprite(source='./images/background.png'))
        self.size = self.children[0].size
        self.add_widget(Label(center=Window.center, text="TAP TO PLAY"))
        
    def on_touch_down(self, *ignore):
        parent = self.parent
        parent.remove_widget(self)
        parent.add_widget(Game())

    
class GameApp(App):
    def build(self):
        game = Game()
        Window.size = game.size
        return game
        top = Widget()
        top.add_widget(Menu())
        Window.size = top.children[0].size
        return top
    
        
if __name__ == '__main__':
    GameApp().run()