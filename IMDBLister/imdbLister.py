#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# import html request modules
import requests
from bs4 import BeautifulSoup
import csv
import pandas as pd
# import Kivy modules
from kivy.app import App
from kivy.lang import Builder
from kivy.core.window import Window
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.label import Label

kv = """
Screen:
    BoxLayout:
        spacing: 10
        orientation: "vertical"

        Label:
            text: "IMDB Lists Query App"
            size_hint: (1.0, 0.1)
            pos_hint: {"center_y": 1}
            font_size: '32sp'
            color: (0, 1, 0, 1)

        TextInput:
            id: input
            text: "Link to one or more (separated by comma) IDMB List(s)"
            size_hint: (0.9, 0.1)
            pos_hint: {"center_y": 1, "x": 0.05}
            font_size: '18sp'
            on_text: app.add_links()

        FloatLayout:
            size_hint: (1.0, 0.15)
            Button:
                text: "Make Query Request"
                size_hint: (0.4, 0.15)
                pos_hint: {"x": 0.05, 'y': 0.7}
                background_normal: 'normal.png'
                background_down: 'down.png'
                color: (0, 0, 0, 1)
                font_size:'18sp'
                on_release: app.update()
                #on_release: app.add_text()

            Button:
                text: "Save Movie List"
                size_hint: (0.4, 0.15)
                pos_hint: {"x": 0.55, 'y': 0.7}
                background_normal: 'normal.png'
                background_down: 'down.png'
                color: (0, 0, 0, 1)
                font_size:'18sp'
                on_release: app.save()

        ScrollView:
            id: scroll_view
            always_overscroll: False
            BoxLayout:
                size_hint_y: None
                height: self.minimum_height
                orientation: 'vertical'
                Label:
                    id: label
                    size_hint_y: None
                    text_size: self.width, None
                    height: self.texture_size[1]
                    font_size: '18sp'
                    color: (0, 1, 0, 1)

"""


class IMDBLister(App):
    title = 'List all movies (title, year, rating) of imdb lists'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.text_counter = 0
        self.urls = ""
        self.url_list = []

    def build(self):
        return Builder.load_string(kv)

    def get_query_list(self):
        """Creates query list"""
        self.urls.strip(" ")
        self.url_list = self.urls.split(",")
        return self.url_list

    def make_query(self, list):
        """Takes list of one or more links and does request."""
        self.urls_list = list
        self.all_movies = []
        self.movies = []
        self.memo = set()
        for link in self.urls_list:
            # Check for the link
            self.response = requests.get(link)
            # Parse recieved html page
            self.soup = BeautifulSoup(self.response.content, "html.parser")
            self.movies_html = self.soup.find_all(
                "div", class_="lister-item-content")
            for movie in self.movies_html:
                title = movie.find("h3").find("a").string
                title = str(title).strip("\n")
                year = movie.find("h3").find(
                    "span", class_="lister-item-year").string
                year = str(year).strip("\n")
                description = movie.find("p", class_="").string
                description = str(description).strip("\n")
                rating = movie.find(
                    "div", class_="ipl-rating-widget").find("span", class_="ipl-rating-star__rating").string
                rating = str(rating).strip("\n")
                entry = {"Title": title, "Year": year,
                         "Description": description, "Rating": rating}
                print(entry)
                self.all_movies.append(entry)
        for sub in self.all_movies:
            if sub["Title"] not in self.memo:
                self.movies.append(sub)
                self.memo.add(sub["Title"])
        return self.movies

    def add_links(self):
        self.urls = "http://"
        self.urls = self.root.ids.input.text

    def update(self):
        self.url_list = []
        self.links_list = self.get_query_list()
        if len(self.links_list) > 0 and self.urls != "":
            self.html_resp = self.make_query(self.links_list)
            # change the label to match the response
            self.display = ""
            self.counter = 0
            for item in self.html_resp:
                if item.get('Description') == "None":
                    item['Description'] = "No Description"
                elif item.get('Rating') == "None":
                    item['Rating'] = "No Rating"
                self.display = self.display + \
                    f"{self.counter+1}: '{item.get('Title')}' {item.get('Year')}\n{item.get('Description')}\nIMDB Rating: {item.get('Rating')}\n\n"
                self.counter += 1
            self.root.ids.label.text = self.display
            # Return response dictionary to be available for saving to csv
            return self.html_resp
        else:
            self.display = "You need give a link to one or more lists first"
            self.root.ids.label.text = self.display

    def save(self):
        if len(self.html_resp) == 0:
            self.query_results.text = "You need to create a list first."
            print("You need to create a list first.")
        else:
            """Writes generated movie list to csv file"""
            field_names = ["Title", "Year", "Description", "Rating"]
            with open('IMDB_Movielist.csv', 'w') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=field_names)
                writer.writeheader()
                writer.writerows(self.html_resp)
            print("Movie List printed.")


if __name__ == "__main__":
    IMDBLister().run()
