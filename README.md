# python_kivy_examples

This are examples of Python programs, that I created in order to learn Python and Kivy.

### IMDBLister

IMDBLister >> Program scrapes data from IMDB Lists, create a list of movies with title, year, description and ratings. This list can be saved as .csv

<img src="img/IMDBLister.png" alt="alt text" title="image Title" width="600"/>

---

### Flappy Calvin

FlappyCalvin >> Little game inspired by "Life" (Scifi Horror). Avoid obstacles by moving Calvin up and down with W (Up) and S (Down), while pressing LMB (Left Mouse Button) to move forward.

<img src="img/FlappyCalvin.png" alt="alt text" title="image Title" width="600"/>
